import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {Post} from '../post.model';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { PostService } from '../post.service';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {mimeType} from './mime-type.validator';



@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit {
enteredTitle = '';
enteredContent = '';
 private mode = 'create';
  postId: string;
  post: Post;
  form: FormGroup;
  constructor(public postservice: PostService, public route: ActivatedRoute) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(3)]
      }),
      contactNumber: new FormControl(null, {validators: [Validators.required]}),
      email: new FormControl(null, {validators: [Validators.required]
    })
    });
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('postId')) {
        this.mode = 'edit';
        this.postId = paramMap.get('postId');
        this.postservice.editPost(this.postId)
        .subscribe(postData => {
          this.post = {id: postData._id, name: postData.name, contactNumber: postData.contactNumber, email: postData.email};
          console.log(this.post);
          this.form.setValue({
            name: this.post.name, contactNumber: this.post.contactNumber, email: this.post.email
          });
        });
      } else {
        this.mode = 'create';
        this.postId = null;
      }
    });
  }

  onSavePost() {
    if (this.form.invalid) {
      return;
    }
    if (this.mode === 'create') {
      console.log(this.form.value.name, this.form.value.contactNumber, this.form.value.email);
      this.postservice.addPost(this.form.value.name, this.form.value.contactNumber, this.form.value.email);
    } else {
      this.postservice.updatePost(
        this.postId,
        this.form.value.name,
        this.form.value.contactNumber,
        this.form.value.email
      );
    }
    this.form.reset();

  }

}
