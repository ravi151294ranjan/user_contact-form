export interface Post {
    id: string;
    name: string;
    contactNumber: string;
    email: string;
}
