import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Post } from './post.model';
import {Subject, from} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {

 private  posts: Post[] = [];
 private postUpdated = new Subject<Post[]>();

  constructor(private http: HttpClient) { }

  getPost() {
    this.http.get<{message: string , posts: any }>('http://localhost:3000/api/posts')
    .pipe(map((postData) => {
      return postData.posts.map(post => {
        return {
          name : post.name,
          contactNumber : post.contactNumber,
          id : post._id,
          email : post.email
        };
      });
    }))
    .subscribe((transformedPost) => {
      this.posts = transformedPost;
      this.postUpdated.next([...this.posts]);
    });
  }

  getPostUpdatedListner() {
    return this.postUpdated.asObservable();
  }

  addPost(name: string, contactNumber: string, email: string) {
    // tslint:disable-next-line:object-literal-shorthand
    const postData: any = { name: name, contactNumber: contactNumber, email: email };
    this.http.post<{message: string, post: Post}>('http://localhost:3000/api/user', postData)
    .subscribe((responseData) => {
      const post: Post = {
        id: responseData.post.id,
        // tslint:disable-next-line:object-literal-shorthand
        name: name,
        // tslint:disable-next-line:object-literal-shorthand
        contactNumber: contactNumber,
        // tslint:disable-next-line:object-literal-shorthand
        email: email
      };
      this.posts.push(post);
      this.postUpdated.next([...this.posts]);
    });
  }

  editPost(postId: string) {
    return this.http.get<{_id: string, name: string, contactNumber: string, email: string}>('http://localhost:3000/api/posts/' + postId);
  }

  updatePost(id: string, name: string, contactNumber: string, email: string) {
    // tslint:disable-next-line:object-literal-shorthand
    const post: Post = {id: id, name: name, contactNumber: contactNumber, email: email };
    this.http.put('http://localhost:3000/api/posts/' + id, post)
    .subscribe(response => {
      const updatedPosts = [...this.posts];
      const oldPostIndex = updatedPosts.findIndex(p => p.id === post.id);
      updatedPosts[oldPostIndex] = post;
      this.posts = updatedPosts;
      this.postUpdated.next([...this.posts]);
    });
  }

  daletePost(postId: string) {
    this.http.delete('http://localhost:3000/api/posts/' + postId)
    .subscribe(() => {
      const updatedPosts = this.posts.filter(post => post.id !== postId);
      this.posts = updatedPosts;
      this.postUpdated.next([...this.posts]);
    });
  }
}
