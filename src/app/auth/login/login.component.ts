import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public authservice: AuthService) { }

  ngOnInit(): void {
  }

  onLogin(ngForm: NgForm) {
    if (ngForm.invalid) {
      return;
    }
    this.authservice.loginUser(ngForm.value.email, ngForm.value.password);
  }

}
