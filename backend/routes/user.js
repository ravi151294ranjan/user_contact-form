const express= require('express');
const User = require('../models/user')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const router= express.Router();

router.post ("/user/signup",(req, res, next)=> {
    console.log(req.body);
    // bcrypt.hash(req.body.password, 10)
    // .then(hash => {
    //     const user = new User({
    //         email: req.body.email,
    //         password: hash
    //     });
    //     user.save()
    //     .then(result => {
    //         res.status(201).json({
    //             message: 'User Created',
    //             result: result
    //         });
    //     })
    //     .catch(err =>{
    //         res.status(500).json({
    //             error: err
    //         });
    //     });
    // });
    const schema= Joi.object().keys({
        email : Joi.string().trim().email().required(),
        password : Joi.string().min(5).max(10).required()
    });

    Joi.validate(req.body, schema, (err, result)=>{
        if(err){
            res.status(500).send('an error has occured')
        }
        bcrypt.hash(result.password, 10)
        .then(hash => {
            const user = new User({
                email: result.email,
                password: hash
            });
            user.save()
            .then(result => {
                res.status(201).json({
                    message: 'User Signed In',
                    result: result
                });
            })
            .catch(err =>{
                res.status(500).json({
                    message: " Authentication Failed"
                });
            });
        });

    })

});

router.post("/user/login", (req, res, next) =>{
    const schema= Joi.object().keys({
        email : Joi.string().trim().email().required(),
        password : Joi.string().min(5).max(10).required()
    });

    Joi.validate(req.body, schema, (err, result)=>{

    })
    let fetchedUser;
    User.findOne({email: req.body.email}).then(user => {
        console.log(user)
        if(!user){
            return res.status(401).json({
                message: "Auth Failed"
            });
        }
        fetchedUser=user;
        return bcrypt.compare(req.body.password, user.password);
    })
    .then(result =>{
        console.log(result);
        if(!result){
            return res.status(401).json({
                message: "Authentication failed"
            });
        }
        const token = jwt.sign({email: fetchedUser.email, userId: fetchedUser._id}, 'secret_this_should_be_longer', {expiresIn: "1h"});
        console.log(token);
        res.status(200).json({
            token: token,
            expiresIn:3600
        });
    })
    .catch(err => {
        return res.status(401).json({
            message: "Authentication Failed"
        });
    })
})

module.exports= router;