const express= require('express');
const multer= require("multer");
const Joi= require('joi');
const Post= require('../models/post');
const checkAuth= require('../middleware/check-auth');

const router= express.Router();

router.post("/user", (req, res, next)=>{
    if(checkAuth) {
        const schema= Joi.object().keys({
            name : Joi.string().trim().required(),
            contactNumber : Joi.string().max(10).required(),
            email : Joi.string().trim().email().required()
        });
    
        Joi.validate(req.body, schema, (err, result) =>{
            if(err){
                res.status(500).json({
                    message: "Authentication Failed"
                })
            }else{
                const post= new Post({
                    name: result.name,
                    contactNumber: result.contactNumber,
                    email: result.email,
                   // creator: req.checkAuth.userData.userId
                })
                post.save().then( createdPost => {
            
                    res.status(201).json(
                        {
                            post: {
                                ...createdPost,
                                id: createdPost._id
                            },
                            messsage: "data added successfully"
                        });
            
                }).catch(error => {
                    res.status(500).json({
                        message: "Authentication Failed"
                    })
                })
            }
    
        })

    }
});

router.get("/posts",(req, res, next)=>{
    Post.find()
    .then((documents) =>{
        res.status(200).json(
            {
                message:"posts fetched successfully",
                posts:documents
            });

    });
});

router.get("/posts/:id", (req, res, next) => {
    console.log("Inside edit");
    Post.findById(req.params.id).then(post => {
        console.log(post);
        if(post) {
            res.status(200).json(post);
        }
        else{
            res.status(404).json({message : 'post not found'});
        }
    }).catch(error => {
        res.status(500).json({
            message: "Authentication Failed"
        })
    })
});

router.put("/posts/:id",(req, res, next) =>{
    console.log("Inside update")
    if(checkAuth){
       // var Query= {name: req.body.name,_id: req.params.id}
        const post = new Post({
            _id: req.body.id,
            name: req.body.name,
            contactNumber: req.body.contactNumber,
            email: req.body.email,
            creator: req.UserData.userId
        });
        Post.updateOne({_id: req.params.id}, post).then((result)=> {
            if(result.n > 0){
                res.status(201).json({
                    message: "posts updated successfully",
                })
            }else
            res.status(401).json({message: "Authentication Failed"})
        }).catch(error => {
            res.status(500).json({
                message: "Authentication Failed"
            })
        })

    }
});

router.delete("/posts/:id",(req, res, next) => {
    if(checkAuth) {
        Post.deleteOne({_id: req.params.id}).then((result)=>{
            res.status(200).json({
                message : "Post Delated Succesfuly"
            });
        }).catch(error => {
            res.status(500).json({
                message: "Authentication Failed"
            })
        })
    }
} );

module.exports=router;