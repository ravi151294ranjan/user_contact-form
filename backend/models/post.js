const mongoose = require('mongoose');

const postSchema= mongoose.Schema({
    name: {type: String, required: true},
    contactNumber: {type: String, required: true},
    email: {type: String, required: true},
    creator: {type: mongoose.Schema.Types.ObjectId, ref: "User", required: false}
});

module.exports=mongoose.model('Post', postSchema)