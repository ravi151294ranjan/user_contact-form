const express = require('express');
const path = require('path');
const bodyparser= require('body-parser');
const postRouter= require('./routes/posts');
const userRouter= require('./routes/user');
const app = express();
const mongoose= require('mongoose');
//require('dotenv').config()


//mongoose.connect("process.env.MONGOURI")
mongoose.connect("mongodb+srv://ravi:ravi0807@sandbox-vyywo.mongodb.net/akrivia_req?retryWrites=true&w=majority")
.then(() => {
    console.log("connected to the database !");
})
.catch(() =>{
    console.log("connection failed!");
})

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: false}));

app.use((req, res, next) =>{
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers',
    "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.setHeader('Access-Control-Allow-Methods',
    "GET, POST, PATCH, PUT, DELETE, OPTIONS");
    next();

});

app.use("/api",postRouter);
app.use("/api",userRouter);



module.exports=app;